/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unl.cic.poo.geometry;

/**
 *
 * @author poo
 */
public class Sphere {
    
    //Variable de Clase
    static final double PI = 3.1416;
    static int countInstances = 0;
    
    //Variable de instancia
    double radius;
    
    
   public double volumen(){
       return 4.0/3.0*PI*Math.pow(radius, 3.0);
   }
   
   public double diameter(){
       return this.radius*2;
   }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
    

    public static int getCountInstances() {
        return countInstances;
    }

    public static void setCountInstances(int countInstances) {
        Sphere.countInstances = countInstances;
    }
   
    
}
