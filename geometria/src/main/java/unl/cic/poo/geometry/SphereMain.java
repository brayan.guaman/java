/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unl.cic.poo.geometry;

/**
 *
 * @author poo
 */
public class SphereMain {
    
    public static void main(String[] args){
    
        Sphere soccerBall = new Sphere();
        Sphere bascketBall = new Sphere();
        
        
        
        soccerBall.radius = 5.0;
        Sphere.countInstances++;
        
        bascketBall.radius = 7.0;
        Sphere.countInstances++;
        
        System.out.println("SoccerBall");
        System.out.println("Volumen soccerBall: "+soccerBall.volumen());
        System.out.println("Diameter soccerBall: "+soccerBall.diameter());
        System.out.println("Radius soccerBall: "+soccerBall.getRadius() );
        System.out.println("Instancias: "+ Sphere.getCountInstances() );
        
        System.out.println("SoccerBall");
        System.out.println("Volumen soccerBall: "+bascketBall.volumen());
        System.out.println("Diameter soccerBall: "+bascketBall.diameter());
        System.out.println("Radius soccerBall: "+bascketBall.getRadius() );
        System.out.println("Instancias: "+ Sphere.getCountInstances() );
        
        
    
    }
    
}
