/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unl.cic.poo.main;

import unl.cic.poo.geometry.Sphere;

/**
 *
 * @author poo
 */
public class SphereMain {
        public static void main(String[] args){
    
        Sphere soccerBall = new Sphere();
        Sphere bascketBall = new Sphere();
        
        
        
        soccerBall.setRadius(5.0);
        Sphere.setCountInstances(Sphere.getCountInstances()+1);
        
        bascketBall.setRadius(7.0);
        Sphere.setCountInstances(Sphere.getCountInstances()+1);
        
        System.out.println("SoccerBall");
        System.out.println("Volumen soccerBall: "+soccerBall.volumen());
        System.out.println("Diameter soccerBall: "+soccerBall.diameter());
        System.out.println("Radius soccerBall: "+soccerBall.getRadius() );
        System.out.println("Instancias: "+ Sphere.getCountInstances() );
        
        System.out.println("SoccerBall");
        System.out.println("Volumen soccerBall: "+bascketBall.volumen());
        System.out.println("Diameter soccerBall: "+bascketBall.diameter());
        System.out.println("Radius soccerBall: "+bascketBall.getRadius() );
        System.out.println("Instancias: "+ Sphere.getCountInstances() );
        
        
    
    }
}
